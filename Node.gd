extends Node

var socket: WebSocketClient
var peer

func _ready():
	socket = WebSocketClient.new()
	socket.connect("connection_established", self, "connected")
	socket.connect("connection_closed", self, "closed")
	socket.connect("server_close_request", self, "disconected")
	socket.connect("connection_error", self, "error")
	socket.connect("data_received", self, "received")
	socket.connect_to_url("localhost:9001")
	
	$Button.connect("pressed", self, "send_message")

func connected(protocol):
	print("connected ", protocol)
	peer = socket.get_peer(1)

func error():
	print("error")

func disconected(code, reason):
	print("disconnected ", code, " ", reason)

func closed(was_clean_close):
	print("closed ", was_clean_close)

func received():
	print(peer.get_var())
	print("received")

func send_message():
	var json = to_json({"aaa": 100})
	peer.put_var(json)

func _process(_delta):
	socket.poll()
